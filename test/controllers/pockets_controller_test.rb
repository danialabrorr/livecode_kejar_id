require 'test_helper'

class PocketsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get pockets_index_url
    assert_response :success
  end

  test "should get new" do
    get pockets_new_url
    assert_response :success
  end

  test "should get edit" do
    get pockets_edit_url
    assert_response :success
  end

end
