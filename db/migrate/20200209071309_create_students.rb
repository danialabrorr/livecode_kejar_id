class CreateStudents < ActiveRecord::Migration[6.0]
  def up
    create_table :students do |t|
      t.string :name, :default => "belum ada nama"
      t.string :username, :default => "belum ada nama"
      t.integer :age, :default => 0
      t.string :kelas, :default => "belum ada kelas"
      t.text :address, :default => "alamat kosong"
      t.string :city, :default => "alamat kosong"
      t.integer :nik, :default => 0

      t.timestamps
    end
  end
  def down
    drop_table :students
  end
end
