class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.bigint :nik, :default => 0
      t.string :name, :default => "belum ada nama"
      t.integer :age, :default => 0
      t.string :kelas, :default => "belum ada kelas"
      t.string :mapel, :default => "tidak ada mapel"

      t.timestamps
    end
  end
  def down
    drop_table :teachers
  end
end
