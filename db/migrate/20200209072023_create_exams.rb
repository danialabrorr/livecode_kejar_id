class CreateExams < ActiveRecord::Migration[6.0]
  def up
    create_table :exams do |t|
      t.string :title, :default => "belum ada judul"
      t.string :mapel, :default => "belum ada mapel"
      t.string :duration, :default => "0"
      t.integer :nilai, :default => 0
      t.integer :status, :default => 0
      t.string :label, :default => "-"
      t.integer :student_id

      t.timestamps
    end
  end

  def down
    drop_table :exams
  end

end
