class ChangeDataTypeExam < ActiveRecord::Migration[6.0]
  def change
    change_table :exams do |t|
        t.change :status, :string
     end
  end
end
