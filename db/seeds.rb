# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Pocket.create(balance: 50000, student_id: 3, teacher_id: 2)
Pocket.create(balance: 94000, student_id: 3, teacher_id: 2)
Pocket.create(balance: 90200, student_id: 3, teacher_id: 2)
Pocket.create(balance: 10000, student_id: 3, teacher_id: 2)
