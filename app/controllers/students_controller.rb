class StudentsController < ApplicationController

    def index       
        @student = Student.all
      end
  
      def show        
        id = params[:id]  
        @student = Student.find(id)       
      end 
  
      def new
        @student = Student.new
      end
      
      def create  
        student = Student.new(student_params)
        student.save
        flash[:notice] = 'student has been created'
        redirect_to students_path
      end
      
      def edit    
        id = params[:id]  
        @student = Student.find(id)
      end
      
      def update      
        @student = Student.find(params[:id])
        @student.update(student_params)
        flash[:notice] = 'student has been updated'
        redirect_to student_path(@student)
      end
      
      def destroy     
        @student = Student.find(params[:id])
        @student.destroy
        flash[:notice] = 'student has been deleted'
        redirect_to students_path
      end
          
      private 
      def student_params
          params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nik)
      end
  

end
