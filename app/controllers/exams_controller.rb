class ExamsController < ApplicationController

        def index       
            @exam = Exam.all
          end
      
          def show        
            id = params[:id]  
            @exam = Exam.find(id)       
          end 
      
          def new
            @exam = Exam.new
          end
          
          def create  
            exam = Exam.new(exam_params)
            exam.save
            flash[:notice] = 'exam has been created'
            redirect_to exams_path
          end
          
          def edit    
            id = params[:id]  
            @exam = Exam.find(id)
          end
          
          def update      
            @exam = Exam.find(params[:id])
            @exam.update(exam_params)
            flash[:notice] = 'exam has been updated'
            redirect_to exam_path(@exam)
          end
          
          def destroy     
            @exam = Exam.find(params[:id])
            @exam.destroy
            flash[:notice] = 'exam has been deleted'
            redirect_to exams_path
          end
              
          private 
          def exam_params
              params.require(:exam).permit(:title, :mapel, :duration, :nilai, :status, :label, :student_id)
          end
    
end
