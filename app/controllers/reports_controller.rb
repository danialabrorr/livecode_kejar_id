class ReportsController < ApplicationController


    def index       
        @report = Report.all
      end
  
    def show        
        id = params[:id]  
        @report = Report.find(id)       
    end 

    def new
        @report = Report.new
    end

    def create
        @report = Report.new(params[:id])
        if @report.save
          flash[:success] = "Report successfully created"
        end
          redirect_to reports_path
    end
    
    def edit    
        id = params[:id]  
        @report = Report.find(id)
      end
      
      def update      
        @report = Report.find(params[:id])
        @report.update(report_params)
        flash[:notice] = 'report has been updated'
        redirect_to report_path(@report)
      end
      
      def destroy     
        @report = Report.find(params[:id])
        @report.destroy
        flash[:notice] = 'report has been deleted'
        redirect_to reports_path
      end
          
      private 
      def report_params
          params.require(:report).permit(:title, :hasil, :mapel, :teacher_id, :student_id, :date)
      end
  
    
end
