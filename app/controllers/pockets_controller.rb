class PocketsController < ApplicationController
    def index       
      @pocket = Pocket.all
    end

    def show        
      id = params[:id]  
      @pocket = Pocket.find(id)       
    end 

    def new
      @pocket = Pocket.new
    end
    
    def create  
      pocket = Pocket.new(pocket_params)
      pocket.save
      flash[:notice] = 'pocket has been created'
      redirect_to pockets_path
    end
    
    def edit    
      id = params[:id]  
      @pocket = Pocket.find(id)
    end
    
    def update      
      @pocket = Pocket.find(params[:id])
      @pocket.update(pocket_params)
      flash[:notice] = 'pocket has been updated'
      redirect_to pocket_path(@pocket)
    end
    
    def destroy     
      @pocket = Pocket.find(params[:id])
      @pocket.destroy
      flash[:notice] = 'pocket has been deleted'
      redirect_to pockets_path
    end
        
    private 
    def pocket_params
        params.require(:pocket).permit(:balance, :student_id, :teacher_id)
    end

end
