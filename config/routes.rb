Rails.application.routes.draw do
  get 'pockets/index'
  get 'pockets/new'
  get 'pockets/edit'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :students
  resources :exams
  resources :teachers
  resources :reports
  resources :pockets
end
